<?php

$guest = [
    '/',
    '/login/login',
    '/login/index',
    '/user/recover',
    '/git/bbHook'
];

$level2 = [
    '/user/index',
];

//can also see everything level2 can see
$level1 = array_merge($level2, [
    '/login/logout',
    '/user/form*',
    '/user/delete*',
    '/',
]);

return [
    0 => $guest,
    1 => $level1,
    2 => $level2
];