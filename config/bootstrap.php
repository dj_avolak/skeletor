<?php

use Laminas\Session\SessionManager;
use Laminas\Session\ManagerInterface;
use Laminas\Session\Config\SessionConfig;
use Monolog\ErrorHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface as Logger;
use Tamtamchik\SimpleFlash\Flash;
use Laminas\Config\Config;
use Skeleton\Acl\Acl;

$containerBuilder = new \DI\ContainerBuilder;
/* @var \DI\Container $container */
$container = $containerBuilder
//    ->addDefinitions(require_once __DIR__ . '/config_web.php')
    ->build();

//@TODO setup caching
$container->set(\FastRoute\Dispatcher::class, function() {
    $routeList = require __DIR__.'/../config/routes.php';

    /** @var \FastRoute\Dispatcher $dispatcher */
    return FastRoute\simpleDispatcher(
        function (\FastRoute\RouteCollector $r) use ($routeList) {
            foreach ($routeList as $routeDef) {
                $r->addRoute($routeDef[0], $routeDef[1], $routeDef[2]);
            }
        }
    );
});

$container->set(Acl::class, function() {
    return new Acl(require __DIR__.'/../config/acl.php');
});

$container->set(Config::class, function() {
    $params = include(APP_PATH . "/../config/config.php");
    $config = new \Laminas\Config\Config($params);
    $config = $config->merge(new \Laminas\Config\Config(include(APP_PATH . "/../config/config-local.php")));

    return $config;
});

$container->set(Flash::class, function () use ($container) {
    //session needs to be started for flash
    $container->get(ManagerInterface::class);
    $flash = new Flash();

    return $flash;
});

$container->set(ManagerInterface::class, function() {
    $sessionConfig = new SessionConfig();
    $sessionConfig->setOptions([
        'remember_me_seconds' => 2592000, //2592000, // 30 * 24 * 60 * 60 = 30 days
        'use_cookies'         => true,
        'cookie_httponly'     => true,
        'name'                => 'wellsite',
    ]);

    $session = new SessionManager($sessionConfig);
    $session->start();

    return $session;
});

$container->set(Logger::class, function() {
    $logger = new \Monolog\Logger('skeletonlog');

    $date = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
    $logDir = APP_PATH . '/../data/logs/' . $date->format('Y') . '-' . $date->format('m');
    $logFile = $logDir . '/' . gethostname() . '-' . $date->format('d') . '.log';
    $debugLog = APP_PATH . '/../data/logs/debug.log';
    // create dir or file if needed
    if (!is_dir($logDir)) {
        mkdir($logDir);
    }
    if (!file_exists($logFile)) {
        touch($logFile);
    }

    $logger->pushHandler(
        new StreamHandler($logFile)
    );
    $logger->pushHandler(
        new StreamHandler(
            $debugLog,
            \Monolog\Logger::DEBUG
        )
    );

    $logger->pushHandler(new BrowserConsoleHandler());

    ErrorHandler::register($logger);

    return $logger;
});

$container->set(DateTime::class, function() use ($container) {
    return new \DateTime('now', new DateTimeZone('Europe/Belgrade'));
});

return $container;