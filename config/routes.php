<?php

/**
 * Define routes here.
 *
 * Routes follow the format:
 *
 * [METHOD, ROUTE, CALLABLE]
 *
 * Routes can use optional segments and regular expressions. See nikic/fastroute
 */

return [
    // Basic example routes. When controller is used without method (as string),
    // it needs to have a magic __invoke method defined
    ['GET', '/', 'Skeleton\Controller\IndexController'],
    [['GET', 'POST'], '/login/{action}', 'Skeleton\Controller\LoginController'],
    [['GET', 'POST'], '/user/{action}[/{userId}]', 'Skeleton\Controller\UserController'],
    ['GET', '/git/bb-hook', 'Skeleton\Controller\GitController'],
];
