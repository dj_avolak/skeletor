<?php
declare(strict_types = 1);
namespace Skeletor\Behaviors;

trait Arrayable
{
    public function toArray($debug = false)
    {
        $data = [];
        foreach (get_object_vars($this) as $name => $value) {
            if (is_object($value)) {
                if (method_exists($value, 'getArrayCopy')) {
                    $subModels = [];
                    foreach ($value->getArrayCopy() as $subModel) {
//                        $subModels[] = $subModel->toArray();
                        $subModels[] = $subModel;
                    }
                    $data[$name] = $subModels;
                } elseif (method_exists($value, 'toArray')) {
                    $data[$name] = $value->toArray();
                } elseif (get_class($value) === "NumberFormatter") {
                    continue;
                } elseif ($value instanceOf \DateTime) {
                    $data[$name] = $value->format('d.m.Y');
                } else {
                    throw new \InvalidArgumentException('Object type is not recognized.');
                }
            } elseif (is_array($value)) {
                $data[$name] = [];
                foreach ($value as $type => $rows) {
                    if (is_string($type) && is_array($rows)) {
                        foreach ($rows as $key => $row) {
                            $data[$name][$type][$key] = $row->toArray();
                        }
                    } else {
                        if (!is_object($rows)) {
                            $data[$name][] = $rows;
                        } elseif ($rows instanceOf \DateTime) {
                            $data[$name][] = $rows->format('d.m.Y');
                        } else {
                            $data[$name][] = $rows->toArray();
                        }
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        return $data;
    }
}