<?php
declare(strict_types = 1);
namespace Skeletor\Mapper;

interface MapperInterface
{
    function insert($modelData);

    function update($modelData);

    function fetchAll($params = array(), int $limit = null);

    function delete(int $modelId);
}
