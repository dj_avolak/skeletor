<?php
declare(strict_types = 1);
namespace Skeletor\Mapper;

/**
 * Class MysqlCrudMapper.
 * Represents default mysql (single table, no relations) crud operations.
 *
 * @package Skeletor\Mapper
 */
class MysqlCrudMapper implements MapperInterface
{
    const PRIMARY_KEY_SUFFIX = 'Id';

    /**
     * @var \PDO
     */
    protected $driver;

    /**
     * @var
     */
    protected $tableName;

    /**
     * @var string
     */
    protected $primaryKeyName;

    /**
     * BaseMapper constructor.
     * @param \PDO $pdo
     * @param $tableName
     */
    protected function __construct(\PDO $pdo, $tableName, $primaryKeyName = null)
    {
        $this->driver = $pdo;
        $this->tableName = $tableName;
        $this->primaryKeyName = $primaryKeyName;
        if (!$primaryKeyName) {
            $this->primaryKeyName = $tableName . self::PRIMARY_KEY_SUFFIX;
        }
    }

    /**
     * Expects an associative array with (at least) required keys as fields names.
     * Does not care about data validation.
     *
     * @param $columnData
     * @return int
     * @throws \Exception
     */
    public function insert($columnData): int
    {
        foreach ($columnData as $key => $value) {
            if (is_object($value) && get_class($value) === \Skeletor\Model\Model::class) {
                $columnData[$key . 'Id'] = $value->getId();
                unset($columnData[$key]);
            }
        }

        unset($columnData[$this->primaryKeyName]);
        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)", $this->tableName, implode(', ', array_keys($columnData)),
            ':' . implode(', :', array_keys($columnData))
        );
        $statement = $this->driver->prepare($sql);
        foreach ($columnData as $key => $value) {
            $statement->bindValue(":$key", $value);
        }
        if (!$statement->execute()) {
            throw new \Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
        }

        return (int) $this->driver->lastInsertId();
    }

    public function updateField($field, $value, $entityId): boolean
    {
        $set = $field . '=:' . $field;
        $sql = sprintf(
            "UPDATE %s SET %s WHERE %s = :%s", $this->tableName, $set, $this->primaryKeyName,
            $this->primaryKeyName
        );
        $statement = $this->driver->prepare($sql);
        $statement->bindValue(":$field", $value);
        $statement->bindValue(":$this->primaryKeyName", $entityId);
    }

    /**
     * @param $columnData
     * @return int
     * @throws \Exception
     */
    public function update($columnData): int
    {
        $primaryKeyValue = $columnData[$this->primaryKeyName];
        unset($columnData[$this->primaryKeyName]);

        $updateSql = '';
        $firstLoop = false;
        foreach ($columnData as $key => $value){
            if ($firstLoop === false){
                $updateSql = $key . ' = :' . $key;
                $firstLoop = true;
            }
            $updateSql .= ', '.$key . ' = :' . $key;
        }
      $sql = sprintf(
            "UPDATE %s SET %s WHERE %s = :%s", $this->tableName, $updateSql, $this->primaryKeyName,
            $this->primaryKeyName
        );
        $statement = $this->driver->prepare($sql);
        foreach ($columnData as $key => $value) {
            $statement->bindValue(":$key", $value);
        }
        $statement->bindValue(":$this->primaryKeyName", $primaryKeyValue);
        if (!$statement->execute()) {
            throw new \Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
        }

        return (int) $primaryKeyValue;
    }

    /**
     * @param int $modelId
     * @return array
     */
    public function fetchById(int $modelId): array
    {
        $sql = "SELECT * FROM `{$this->tableName}` WHERE `{$this->primaryKeyName}` = $modelId";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();
        $item = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$item) {
            throw new NotFoundException('Entity not found: ' . $modelId);
        }

        return $item;
    }

    /**
     * @param array $params
     * @param null $limit
     * @return array
     */
    public function fetchAll($params = array(), int $limit = null, $order = null): array
    {
        $sql = "SELECT * FROM `{$this->tableName}` ";
        $i = 0;
        foreach ($params as $name => $value) {
            if ($i === 0) {
                $sql .= " WHERE `{$name}` = '{$value}' ";
            } else {
                $sql .= " AND `{$name}` = '{$value}' ";
            }
            $i++;
        }
        if ($order) {
            $sql .= " ORDER BY {$order['orderBy']} {$order['dir']} ";
        }
        if (isset($limit)) {
            $sql .= " LIMIT " . $limit;
        }
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return  $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $sql = "DELETE FROM `{$this->tableName}` WHERE `{$this->primaryKeyName}` = $id";

        return $this->driver->prepare($sql)->execute();
    }
}
