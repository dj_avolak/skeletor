<?php
declare(strict_types = 1);
namespace Skeletor\Mapper;

/**
 * Created by PhpStorm.
 * User: djavolak
 * Date: 9/11/2017
 * Time: 6:30 PM
 */
class NotFoundException extends \Exception
{

}