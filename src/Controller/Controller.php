<?php
declare(strict_types = 1);
namespace Skeletor\Controller;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest as Request;
use Laminas\Session\SessionManager as Session;
use Tamtamchik\SimpleFlash\Flash;
use Laminas\Config\Config;

class Controller
{
    /**
     * @var \Twig\Environment
     */
    private $template;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    protected $protectedPath = 'admin';

    public function __construct(
        $template,
        Config $config,
        Session $session,
        Flash $flash
    ) {
        $this->template = $template;
        $this->config = $config;
        $this->session = $session;
        $this->flash = $flash;
        $this->response = new Response();
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param callable|null $next
     * @return mixed
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response, callable $next = null)
    {
        if (!$request->getAttribute('action')) {
            throw new \Exception('Action attribute is not set.');
        }
        $this->request = $request;
        $this->response = $response;

        $this->setGlobalVariables();
        if (!method_exists($this, $request->getAttribute('action'))) {
            throw new \Exception(sprintf('Method %s does not exist', $request->getAttribute('action')));
        }
        $response = $this->{$request->getAttribute('action')}();

        return $response;
    }

    /**
     * @return int
     */
    public function getLoggedInUserId(): int
    {
        if (!$this->getSession()->getStorage()->offsetGet('loggedIn')) {
//            throw new \Exception('not logged in exception...');
        }

        return $this->getSession()->getStorage()->offsetGet('loggedIn');
    }

    public function setGlobalVariable($name, $value)
    {
        $this->template->addGlobal($name, $value);
    }

    /**
     * Set global twig variables.
     */
    private function setGlobalVariables()
    {
        if ($this->getSession()->getStorage()->offsetGet('loggedIn')) {
            $this->template->addGlobal('loggedInEmail', $this->getSession()->getStorage()->offsetGet('loggedInEmail'));
        }

        if ($this->flash::hasMessages()) {
            $this->template->addGlobal('messages', $this->flash::display());
        }
        $this->template->addGlobal('loggedIn', $this->getSession()->getStorage()->offsetGet('loggedIn'));
        $this->template->addGlobal('loggedInRole', $this->getSession()->getStorage()->offsetGet('loggedInRole'));
        $this->template->addGlobal('route', $this->getRequest()->getUri()->getPath());
//        $this->twig->addGlobal('pageTitle', $loggedIn);
    }

    /**
     * Return response to client.
     *
     * @param $template
     * @param array $data
     *
     * @return Response
     */
    public function respond($template, $data = []) : Response
    {
        $className = explode('\\', get_class($this));
        $controller = strtolower(str_replace('Controller', '', $className[count($className) - 1]));
        $template = sprintf('%s/%s/%s.twig', $this->protectedPath, $controller, $template);
        try {
            $this->response->getBody()->write($this->template->render($template, ['data' => $data]));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $this->response->getBody()->rewind();

        return $this->response;
    }

    public function respondPartial($template, $data = []) : Response
    {
        $className = explode('\\', get_class($this));
        $controller = strtolower(str_replace('Controller', '', $className[count($className) - 1]));
        $template = sprintf('%s/%s/%s.twig', $this->protectedPath, $controller, $template);
        $this->response->getBody()->write($this->template->render($template, ['data' => $data]));
        $this->response->getBody()->rewind();

        return $this->response;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * @return Session
     */
    public function getSession() : Session
    {
        return $this->session;
    }

    /**
     * @return Flash
     */
    public function getFlash() : Flash
    {
        return $this->flash;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Redirect client to given uri.
     *
     * @param $url
     *
     * @return Response
     */
    public function redirect($url): Response
    {
        if (!strstr($url, 'http')) {
            $url = $this->config->offsetGet('baseUrl') . $url;
        }

        return $this->response->withStatus(302)->withHeader('Location', $url);
    }
}
