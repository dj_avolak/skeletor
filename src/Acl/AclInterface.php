<?php
declare(strict_types = 1);
namespace Skeletor\Acl;

/**
 * Interface AclInterface
 * @package Skeleton\Acl
 */
interface AclInterface
{
    const GUEST_LEVEL = 0;

    /**
     * Returns a role of an object in this system.
     *
     * @return int
     */
    public function getRole();
}