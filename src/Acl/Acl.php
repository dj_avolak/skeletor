<?php
declare(strict_types = 1);
namespace Skeletor\Acl;

use Laminas\Config\Config;
use Laminas\Session\SessionManager;
use WeCare\User\Model\Guest;

/**
 * Class Acl
 * @package Skeleton\Acl
 */
class Acl
{
    /**
     * @var array
     */
    private $aclData;

    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * Acl constructor.
     *
     * @param SessionManager $sessionManager
     * @param Config $config
     * @param $aclData
     */
    public function __construct(SessionManager $sessionManager, Config $config, $aclData)
    {
        $this->aclData = $aclData;
        $this->sessionManager = $sessionManager;
    }

    public function isGuestPath($requestedPath)
    {
        foreach ($this->aclData[AclInterface::GUEST_LEVEL] as $path) {
            $wildcard = strstr($path, '*', true);
            if ($wildcard && strpos($requestedPath, $wildcard) !== false) {
                return true;
            }
            if ($requestedPath === $path) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if an entity can access given path.
     *
     * @param AclInterface $entity
     * @param $requestedPath
     * @return bool
     */
    public function canAccess(AclInterface $entity, $requestedPath): bool
    {
        $level = $entity->getRole();
        foreach ($this->aclData[$level] as $path) {
            $wildcard = strstr($path, '*', true);
            if ($wildcard && strpos($requestedPath, $wildcard) !== false) {
                return true;
            }
            if ($requestedPath === $path) {
                return true;
            }
        }

        return false;
    }
}
