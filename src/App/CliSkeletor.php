<?php
declare(strict_types = 1);
namespace Skeletor\App;

use Psr\Log\LoggerInterface as Logger;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest as Request;

class CliSkeletor
{
    /**
     * @var \DI\Container
     */
    private $dic;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var Logger
     */
    private $logger;

    private $timer;

    /**
     * WebSkeletor constructor.
     *
     * @param \DI\Container $dic
     */
    public function __construct(\DI\Container $dic, Logger $logger)
    {
        $this->dic = $dic;
        $this->logger = $logger;
    }

    public function __invoke(...$params)
    {
        $this->handle($params);
    }

    /**
     * Handle request and dispatch route.
     */
    private function handle($params)
    {
        $map = [
            'Ticket' => \WeCare\Ticket\Controller\TicketController::class,
            'Notification' => \WeCare\Notification\Controller\NotificationController::class,
        ];
//        $this->timer = microtime();
//        $this->logger->debug('init : ' . (microtime() - $this->timer));

        try {
            $controller = $map[$params[0]];
            $method = $params[1];
            $controller = $this->dic->get($controller);
            $this->response = $controller->{$method}();
        } catch (\Exception $e) {
            $this->handleErrors($e);
        }
    }

    /**
     * Handle errors and prepare response object.
     *
     * @TODO send email notification
     *
     * @param \Exception $exception
     */
    public function handleErrors(\Exception $exception)
    {
        $msg = $exception->getMessage();

        switch (get_class($exception)) {
            case \InvalidArgumentException::class:
//                $this->response->getBody()->write($msg);

                break;
            case \Exception::class:

                break;

            default:
                $msg = '<h3>' . $msg . '</h3>' . $exception->getTraceAsString();

                break;
        }

        $this->dic->get(Logger::class)->error($msg);
//        $this->dic->get(Logger::class)->error($exception->getTraceAsString());

        if (!defined(APPLICATION_ENV) && strtolower(getenv('APPLICATION_ENV')) === 'development') {
            $this->response->getBody()->write('<h1>An error has occurred. </h1>' . PHP_EOL);
            $this->response->getBody()->write($msg);
        }
    }

    /**
     * Sends respond back to client.
     *
     */
    public function respond()
    {
        if (!in_array($this->response->getStatusCode(), [205, 304])) {
            $body = $this->response->getBody();
            if ($body->isSeekable()) {
                $body->rewind();
            }
            while (!$body->eof()) {
                echo $body->read(2048);
                if (connection_status() != CONNECTION_NORMAL) {
                    break;
                }
            }
        }
    }
}
