<?php

namespace Test\Skeleton\Controller;

class IndexControllerTest extends \PHPUnit_Framework_TestCase
{

    public function testIndexMethodShouldReturnRedirect()
    {
        $_SESSION = [];
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->getMockForAbstractClass();
        $twigMock = $this->getMockBuilder(\Twig_Environment::class)
            ->getMockForAbstractClass();
        $sessionMock = $this->getMockBuilder(\Zend\Session\ManagerInterface::class)
            ->getMockForAbstractClass();
        $configMock = $this->getMockBuilder(\Zend\Config\Config::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $requestMock = $this->getMockBuilder(\GuzzleHttp\Psr7\ServerRequest::class)
            ->disableOriginalConstructor()
            ->setMethods(['getAttribute'])
            ->getMockForAbstractClass();
        $requestMock->expects(static::once())
            ->method('getAttribute')
            ->willReturn('index');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new \Skeleton\Controller\IndexController($twigMock, $sessionMock, $flashMock, $configMock);
        $response = $controller($requestMock, $response);
        $this->assertSame(302, $response->getStatusCode());
        $this->assertSame('/mainboard/index', $response->getHeader('Location')[0]);
    }
}