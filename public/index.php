<?php

define('APP_PATH', __DIR__ . '/..');

error_reporting(E_ALL);
ini_set('display_errors', 1);

include("../vendor/autoload.php");

/* @var \DI\Container $container */
$container = require APP_PATH . '/config/bootstrap.php';
$app = new \Skeletor\App\WebSkeletor($container, $container->get(\Psr\Log\LoggerInterface::class));
$app->respond();
